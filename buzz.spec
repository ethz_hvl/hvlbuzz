import os
import hvlbuzz
from kivy_deps import sdl2, glew
from PyInstaller.utils.hooks import collect_data_files

src_path = os.path.dirname(hvlbuzz.__file__)

block_cipher = None

a = Analysis(
    ["hvlbuzz/bundled_main.py"],
    pathex=[src_path],
    binaries=[],
    hiddenimports=["win32timezone", "win32file"],
    hookspath=[],
    runtime_hooks=[],
    excludes=[],
    win_no_prefer_redirects=False,
    win_private_assemblies=False,
    cipher=block_cipher,
    noarchive=False,
    datas=[
        ("hvlbuzz/static/font/*", "static/font/"),
        ("hvlbuzz/static/helpdocs/*", "static/helpdocs/"),
        ("hvlbuzz/static/images/*", "static/images/"),
        *collect_data_files("hvlbuzz", include_py_files=True, excludes=["**/__pycache__/*"])
    ]
)
pyz = PYZ(a.pure, a.zipped_data, cipher=block_cipher)
exe = EXE(
    pyz,
    a.scripts,
    [],
    exclude_binaries=True,
    name="buzz",
    debug=False,
    bootloader_ignore_signals=False,
    strip=False,
    upx=True,
    console=False,
)
coll = COLLECT(
    exe,
    Tree(src_path),
    a.binaries,
    a.zipfiles,
    a.datas,
    *[Tree(p) for p in (sdl2.dep_bins + glew.dep_bins)],
    strip=False,
    upx=True,
    upx_exclude=[],
    name="buzz",
)
