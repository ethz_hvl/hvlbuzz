:hide-toc:

.. image:: img/buzz.svg
    :width: 10%
    :class: right-image


Welcome to Buzz's documentation!
================================

.. container:: toc-heading

    .. image:: img/tutorial.svg
       :alt: Tutorial icon
       :class: heading-icon

    Tutorial

.. toctree::
   :maxdepth: 2

   usage/index

.. container:: toc-heading

    .. image:: img/physics.svg
       :alt: Physical Model icon
       :class: heading-icon

    Physical Model

.. toctree::
   :maxdepth: 2

   physical_model/index


.. container:: toc-heading

    .. image:: img/source.svg
       :alt: src icon
       :class: heading-icon

    Source Code Reference


.. toctree::
   :maxdepth: 2

   code_reference
