Magnetic Field
========================

H-field calculation:

The magnetic field is calculated analytically using the Biot-Savart law.

.. math::

   \begin{align}
     r &= \sqrt{(x-x_\mathrm{conductor})^2 + (y-y_\mathrm{conductor})^2},\\
     H_x &= -\frac{I}{2\pi r^2}(y - y_\mathrm{conductor}),\\
     H_y &= \frac{I}{2\pi r^2}(x - x_\mathrm{conductor}),\\
     H &= \sqrt{H_x^2 + H_y^2}.
   \end{align}

B-field calculation:

The quantity that is shown on the graph as the magnetic field is actually the
magnetic flux density.

.. math::

   B = \mu_0 H
