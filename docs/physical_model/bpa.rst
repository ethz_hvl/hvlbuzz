BPA Audible Noise
========================

The following are the AC and DC audible noise empirical formula developed by
the Bonneville Power Administration (BPA).

AC
**

Constants:

.. math::

   k = \begin{cases}
     0,& n<3, \\
     26.4,& n \geq 3.
   \end{cases}

Generated acoustic power:

.. math::

   A = \begin{cases}
     k \log(n) + 55 \log(d) + 120 \log(E_\mathrm{AC}) - 229.7,
       & n<3, \\
     k \log(n) + 55 \log(d) + 120 \log(E_\mathrm{AC}) - 242.7,
       & n \geq 3.
   \end{cases}

Sound pressure level:

.. math::

   P_{50} = A + 114.3 - 11.4\log(R).

Total sound pressure level from all phases:

.. math::

   P_\mathrm{total} = 10 \log\left(\sum_{i=1}^n 10^{\frac{P_{50,i}}{10}}\right).

Fair weather correction factor:

.. math::

   P_\mathrm{fair} = P_\mathrm{foul} - 25.

Rain correction
---------------

See :ref:`the corresponding section in the EPRI section <epri_rain_correction>`

DC
**

Equivalent conductor diameter:

.. math::

   d_\mathrm{eq} = \begin{cases}
     d,& n<3, \\
     d \cdot 0.66 n^{0.64},& n \geq 3.
   \end{cases}

Generated acoustic power:

.. math::

   A = 86 \log(E_\mathrm{DC}) + 40 \log(d_\mathrm{eq}) - 241.9.

Sound pressure level:

.. math::

   P_{50} = A + 114.3 - 11.4\log(R) - 5.8.

Total sound pressure level from all phases:

.. math::

   P_\mathrm{total} = 10 \log\left(\sum_{i=1}^n 10^{\frac{P_{50,i}}{10}}\right).

Foul weather correction factor:

.. math::

   P_\mathrm{foul} = P_\mathrm{fair} - 6.

Seasonal correction factor:

.. math::

   \begin{align}
     P_\mathrm{summer} &= P_\mathrm{fall/spring} + 2,\\
     P_\mathrm{winter} &= P_\mathrm{fall/spring} - 2.
   \end{align}
