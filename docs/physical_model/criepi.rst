CRIEPI Audible Noise
========================

The following is the DC audible noise empirical formula which was developed by
the Central Research Institute of Electric Power Industry of Japan (CRIEPI).

DC
**

Surface gradient at 50dB:

.. math::

   E_{50} = \left(
     \frac{\log(n)}{91}
     + \frac{\log(d)}{19}
     + \frac{1}{2W^2}
     + \frac{1}{151}
   \right)^{-1}.

Surface gradient at 60dB:

.. math::

   E_{60} = \left(
     \frac{\log(n)}{71}
     + \frac{\log(d)}{21}
     + \frac{1}{2W^2}
     + \frac{1}{1906}
   \right)^{-1}.

Sound pressure level:

.. math::

   P_{50} = 50 + 10 \frac{E_{60}}{E_{60}-E_{50}} \left(1-\frac{E_{50}}{E_\mathrm{DC}}\right) - 10\log(R).

Total sound pressure level from all phases:

.. math::

   P_\mathrm{total} = 10 \log\left(\sum_{i=1}^n 10^{\frac{P_{50,i}}{10}}\right).
