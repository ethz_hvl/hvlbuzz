Charge Simulation Method
========================

Conductor placement within a bundle:

The placement depends on the number of conductors and the bundle radius defined
when a new line is added. The line coordinates will be taken as a midpoint
for the conductor bundle.

.. math::

   \begin{align}
    x_{\mathrm{conductor},k} &= x_{\mathrm{line}} + r_{\mathrm{bundle}}
      \cdot \cos \left(\frac{2\pi k}{n_{\mathrm{conductor}}}
                       + \alpha \frac{\pi}{180} \right),
     & 0\leq k \leq n_{\mathrm{conductor}} - 1 \\
    y_{\mathrm{conductor},k} &= y_{\mathrm{line}} + r_{\mathrm{bundle}}
      \cdot \sin \left(\frac{2\pi k}{n_{\mathrm{conductor}}}
                       + \alpha \frac{\pi}{180} \right),
     & 0\leq k \leq n_{\mathrm{conductor}} - 1.
   \end{align}

Contour points on the conductor surfaces:

increasing the number of conductor surfaces increases the accuracy of the
electric field calculation but also calculation times. The number of contour
points can be determined in the Electric Field settings page.

.. math::

   \begin{align}
    x_{\mathrm{contour},k} &= x_{\mathrm{conductor}} + r_{\mathrm{conductor}}
      \cdot \cos \left(\frac{2\pi k}{n_{\mathrm{contour}}} \right),
     & 0\leq k \leq n_{\mathrm{contour}} - 1 \\
    y_{\mathrm{contour},k} &= y_{\mathrm{conductor}} + r_{\mathrm{conductor}}
      \cdot \sin \left(\frac{2\pi k}{n_{\mathrm{contour}}} \right),
     & 0\leq k \leq n_{\mathrm{contour}} - 1.
   \end{align}

Charge radius within the the conductor:

.. math::

   r_\mathrm{charge} =  \frac{r_\mathrm{conductor}}{1 + 2 \sin(\pi/n_\mathrm{contour})}.

Charge placement within conductors:

Every line charge comes in pair with a surface contour point.

.. math::

   \begin{align}
    x_{\mathrm{charge},k} &= x_{\mathrm{conductor}} + r_{\mathrm{charge}}
     \cdot \cos \left(\frac{2\pi k}{n_{\mathrm{contour}}} \right),
     & 0\leq k \leq n_{\mathrm{contour}} - 1 \\
    y_{\mathrm{charge},k} &= y_{\mathrm{conductor}} + r_{\mathrm{charge}}
     \cdot \sin \left(\frac{2\pi k}{n_{\mathrm{contour}}} \right),
     & 0\leq k \leq n_{\mathrm{contour}} - 1.
   \end{align}

Mirror charge placement to simulate the ground with zero potential

The line charge in the conductors and their mirror charges also come in pairs.
The mirror charges are required to simulate the zero potential on ground.

.. math::

   \begin{align}
    x_{\mathrm{mirror},k} &= x_{\mathrm{charge},k} \\
    y_{\mathrm{mirror},k} &= -y_{\mathrm{charge},k}
   \end{align}

Potential coefficients:

.. math::

   p_{ij} = \frac{1}{2\pi \varepsilon_0}
     \log\left(
       \frac{
        |x_{\mathrm{contour},i} - x_{\mathrm{mirror},j}|
       }{
        |x_{\mathrm{contour},i} - x_{\mathrm{charge},j}|
       }
     \right).

AC and DC voltage vectors:

.. math::

   \begin{align}
     [U]_{\mathrm{AC}} &= \begin{cases}
       0, & \mathrm{if\ GND\ or\ DC} \\
       \hat{U}_i \mathrm{e}^{j\theta_i}, &\mathrm{if\ AC}
     \end{cases} \\
     [U]_{\mathrm{DC}} &= \begin{cases}
       0, & \mathrm{if\ GND\ or\ AC} \\
     U_i, &\mathrm{if\ DC}.
     \end{cases}
   \end{align}

AC and DC line charge vectors:

.. math::

   \begin{align}
     [Q]_{\mathrm{AC}} &= [P]^{-1}[U]_{\mathrm{AC}} \\
     [Q]_{\mathrm{DC}} &= [P]^{-1}[U]_{\mathrm{DC}}.
   \end{align}

Electric field on ground:

.. math::

   E_{\mathrm{ground}} = \frac{\lambda}{2\pi \varepsilon_0}
     \left(
       \frac{
        |x_{\mathrm{ground},j} - x_{\mathrm{charge},j}|
       }{
        |x_{\mathrm{ground},j} - x_{\mathrm{charge},j}|^2
       }
       - \frac{
        |x_{\mathrm{ground},j} - x_{\mathrm{mirror},j}|
       }{
        |x_{\mathrm{ground},j} - x_{\mathrm{mirror},j}|^2
       }
     \right).

Electric field on conductor surfaces (surface gradients):

.. math::

   E_{\mathrm{conductor}} = \frac{\lambda}{2\pi \varepsilon_0}
     \left(
       \frac{
        |x_{\mathrm{conductor},j} - x_{\mathrm{charge},j}|
       }{
        |x_{\mathrm{conductor},j} - x_{\mathrm{charge},j}|^2
       }
       - \frac{
        |x_{\mathrm{conductor},j} - x_{\mathrm{mirror},j}|
       }{
        |x_{\mathrm{conductor},j} - x_{\mathrm{mirror},j}|^2
       }
     \right).
