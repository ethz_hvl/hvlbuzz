EPRI Audible Noise
========================

The following are the AC and DC audible noise empirical formula developed by
the Electric Power Research Institute (EPRI).

AC
--

Constants:

.. math::

   K_n = \begin{cases}
     7.5,& n=1, \\
     2.6,& n=2, \\
     0,& n \geq 3.
   \end{cases}

Generated acoustic power:

.. math::

   A = \begin{cases}
     20 \log(n) + 44 \log(d) - \frac{665}{E_\mathrm{AC}} + K_n - 39.1,
       & n<3, \\
     20 \log(n) + 44 \log(d) - \frac{665}{E_\mathrm{AC}} + K_n
       + 22.9\frac{(n-1)d}{D} - 46.4,
       & n \geq 3.
   \end{cases}

L_5 sound pressure level:

.. math::

   P_5 = A + 114.3 - 10 \log(R) - 0.02 R

.. _epri_rain_correction:

Rain rate correction
....................

.. math::

   \begin{align}
     E_c &= \begin{cases}
       \frac{24.4}{d^{0.24}}, & n \leq 8, \\
       \frac{24.4}{d^{0.24}} - 0.25 (n-8), & n > 8, \\
     \end{cases}\\
     \Delta A &= \begin{cases}
       8.2 + \frac{14.2 E_c}{E}, & n < 3, \\
       10.4 + \frac{14.2 E_c}{E} + 8\frac{(n-1)d}{D}, & n \geq 3. \\
     \end{cases}
   \end{align}

L_50 sound pressure level:

.. math::

   P_{50} = P_5 + \Delta A.

Total sound pressure level from all phases:

.. math::

   P_\mathrm{total} = 10 \log\left(\sum_{i=1}^n 10^{\frac{P_{50,i}}{10}}\right).

Fair weather correction factor:

.. math::

   P_\mathrm{fair} = P_\mathrm{foul} - 25.

.. note:: When the option `Use previous field strength independent rain rate
   correction` is set to "on", the constant rain rate correction used in
   the HVLBUZZ program until 2023 is applied (only in the EPRI and BPA
   method). This correction following EMPA report no. 452'574-2, version
   2016 is no longer recommended. According to this document, a curve for the
   dependence of the noise
   level on the rain rate, which is independent of the conductor surface
   field strength, is added to the previously calculated noise
   level for "wet conductors" (rain rate 0.75 mm/h). The curve can be
   found in Figure 6.4.35, `EPRI Transmission Line Reference Book 345 kV
   and above, ed. 2, 1982 <https://www.osti.gov/biblio/5278767>`_ (full
   PDF available `here <https://www.academia.edu/41079824/EPRI_Transmission_Line_Reference_Book_345_kV_and_above>`_).
   Brief justification for the replacement of this rain rate correction
   by a field dependent correction:
   The aforementioned level curve as a function of the rain rate was
   determined on the occasion of an EPRI UHV overhead line project. Phase
   bundles with 8-12 conductors were tested at voltages between 950 and
   1450 kV. The level difference :math:`\Delta A` or :math:`A_{wc}`
   between the sound level for "wet conductors" and "heavy rain" (0.75
   mm/h or 6.5 mm/h) is approx. 4 dB(A) on the curve.
   According to the EPRI calculation formulation for :math:`A_{wc}`, this
   sound level difference only applies with a defined geometry dependent
   conductor surface field strength.

   .. csv-table:: Examples of field strength values for which the
                  previous curve correction is in line with EPRIs
                  definition of :math:`A_{wc}`
      :header: "", "⌀ 25 mm", "⌀ 32 mm"

      "Single conductors and double conductors", "22.9 kV/cm", "21.6 kV/cm"
      "Phase bundles of three conductors", "18.3 kV/cm", "17.0 kV/cm"
      "Phase bundles of four conductors", "18.1 kV/cm", "16.7 kV/cm"

   At lower surface field strength values, the correction made with the
   curve is too small, and at higher field strengths the correction is
   too large. For this reason, the updated formulation according to the
   "EPRI Transmission Line Reference Book 345 kV and Above, ed. 3, 2006"
   is recommended. This correction is made by weighting the field
   strength-dependent relationship for :math:`\Delta A` or :math:`A_{wc}`
   with a logarithmic rain rate function. For this updated rain rate
   correction the switch is set to “off”.

DC
--

Constants:

.. math::

   K_n = \begin{cases}
     7.5,& n=1, \\
     2.6,& n=2, \\
     0,& n \geq 3.
   \end{cases}

Generated acoustic power:

.. math::

   A = 
     124 \log(\tfrac{E}{25}) + 25 \log(\tfrac{d}{4.45}) + 18\log(\tfrac{n}{2}) + K_n - 57.4.

Sound pressure level:

.. math::

   P_{50} = A + 114.3 - 10\log(R) -0.02 R.

Total sound pressure level from all phases:

.. math::

   P_\mathrm{total} = 10 \log\left(\sum_{i=1}^n 10^{\frac{P_{50,i}}{10}}\right).

Foul weather correction factor:

.. math::

   P_\mathrm{fair} = P_\mathrm{foul} - 6.

Seasonal correction factor:

.. math::

   \begin{align}
     P_\mathrm{winter} &= P_\mathrm{summer} - 4,\\
     P_\mathrm{spring} &= P_\mathrm{fall} = P_\mathrm{summer} - 2.
   \end{align}
