******************
Tutorial: HVL-Buzz
******************

+-----------------------------------------------------------------------+
| Software for electric field simulation & prediction of audible noise  |
+=======================================================================+
| **FKH - Fachkommision für Hochspannungsfragen**                       |
|                                                                       |
| Hagenholzstrasse 81                                                   |
|                                                                       |
| CH-8050 Zürich                                                        |
|                                                                       |
| Dr. Henrik Menne                                                      |
|                                                                       |
| Tel.: +41-(0)44 253 62 68                                             |
|                                                                       |
| `www.fkh.ch <http://www.fkh.ch>`__                                    |
|                                                                       |
|*Stand: 11/2023*                                                       |
+-----------------------------------------------------------------------+


Tower Geometry 
===============

In order to calculate the electric and magnetic fields and predict the
audible noise, first, a geometry has to be defined. This can either be
created from scratch or loaded and altered from an existing geometry.

Create Geometry
---------------

In order to create a new geometry, the main menu is accessed using the
three grey dots as in :numref:`main-menu`. Here, among various options, selecting
“New” will open the system designer.

.. figure:: img/main-menu.png
   :name: main-menu

   Main menu

The System Designer as in :numref:`create-system` allows to generate a tower geometry
based on the input of voltage form, current, number and radius of
conductors and bundle, the angle of the bundle and its coordinates.
Special care needs to be given regarding the correct units, as e.g. the
bundle radius is required in mm.

.. figure:: img/create-system.png
   :name: create-system

   Create system

Load/Save Geometry
------------------

Instead of generating a new geometry, an existing geometry can be loaded
using the load button in :numref:`main-menu`. Here, either the explorer window can
be used to find the data, but also the path can be entered directly,
which is required for a change of main directory in :numref:`load-file`.

.. figure:: img/load-file.png
   :name: load-file

   Load file

In a very similar fashion, also the existing data can be saved and the
path is entered either through the explorer or directly as text input in
:numref:`save-file`.

.. figure:: img/save-file.png
   :name: save-file

   Save file

Simulation & Prediction
=======================

Output plots
------------

Based on the input geometry and the simulation results, the distribution
is plotted for AN, EF and MF, as in :numref:`an-distribution`,
:numref:`e-field-distribution`, :numref:`b-field-distribution` and the
results are summarized in a table as in :numref:`result-table`.

.. figure:: img/an-distribution.png
   :name: an-distribution

   AN distribution

.. figure:: img/e-field-distribution.png
   :name: e-field-distribution

   Electric Field Distribution

.. figure:: img/b-field-distribution.png
   :name: b-field-distribution

   Magnetic field distribution

.. figure:: img/result-table.png
   :name: result-table

   Results table

Results & Export
----------------

Besides the live plots, the results can also be exported as figures in
png or pdf. Furthermore, the results can be exported as a table in csv
depending on the selection in the main menu shown in :numref:`main-menu`.

Settings
========

Postprocessing 
---------------

Various settings can be selected for the postprocessing and geometry in
:numref:`tower-settings`. Here, the scaling and axis limits can be
set, as well as the color definitions for the individual systems.

.. figure:: img/tower-settings.png
   :name: tower-settings

   Tower Settings

For the individual curves of AN, EF and MF, the axis settings can be
selected, as well as the units and altitude correction for the AN in
:numref:`noise-settings`, :numref:`e-field-settings` and :numref:`b-field-settings`.

.. figure:: img/noise-settings.png
   :name: noise-settings

   Noise settings

.. figure:: img/e-field-settings.png
   :name: e-field-settings

   Electric field settings

.. figure:: img/b-field-settings.png
   :name: b-field-settings

   Magnetic field settings

For the data export in tables, the delimiter and separator can be
switched according to :numref:`export-settings`.

.. figure:: img/export-settings.png
   :name: export-settings

   Export settings

GUI
---

The graphical user interface based on the Python plugin Kivy allows to
toggle full screen mode, set a FPS limit, rotate the window and many
more, as shown exemplarily in :numref:`gui-settings`.

.. figure:: img/gui-settings.png
   :name: gui-settings

   GUI settings
