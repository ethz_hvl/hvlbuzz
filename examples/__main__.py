#!/bin/env python3
"""example runner.

Runs all example files in the current directory and checks if
csv files before and afterwards are the same up to numerical tolerance.
"""

from __future__ import annotations

import glob
import itertools
import os
import subprocess
import sys
from typing import TYPE_CHECKING

import numpy as np

if TYPE_CHECKING:
    from collections.abc import Iterable

    from numpy.typing import NDArray

if sys.version_info < (3, 11):
    from exceptiongroup import ExceptionGroup


def main() -> None:
    """Entry point of the script.

    Run in diff mode for 2 arguments,
    and run / check output of all examples otherwise.
    """
    args = sys.argv[1:]
    if len(args) > 0:
        try:
            a, b = args
        except ValueError:
            print(f"Expected 0 or 2 arguments, got: {args}", file=sys.stderr)
            sys.exit(1)
        have_mismatches = diff(a, b)
        sys.exit(int(have_mismatches))
    run_examples()


def run_examples() -> None:
    """Search for examples and csv files, run and check."""
    example_dir = os.path.dirname(__file__)
    example_files = glob.glob(os.path.join(example_dir, "**", "*.py"), recursive=True)
    example_files = [
        path
        for path in example_files
        if not os.path.basename(path).startswith("__") and "__pycache__" not in os.path.split(path)
    ]
    csv_files = glob.glob(os.path.join(example_dir, "**", "*.csv"), recursive=True)
    csv_data_before = {path: read_csv(path) for path in csv_files}
    crashes = []
    for example_file in example_files:
        crashes += run_example(example_file, cwd=example_dir)
    if crashes:
        msg = "Examples crashed"
        raise ExceptionGroup(msg, crashes)
    csv_data_after = {path: read_csv(path) for path in csv_files}
    exceptions = list(compare_tables(csv_data_before, csv_data_after))
    if exceptions:
        msg = "Mismatch summary"
        raise ExceptionGroup(msg, exceptions)


def run_example(path: str, cwd: str) -> list[subprocess.CalledProcessError]:
    """Run a single example."""
    try:
        subprocess.check_call([sys.executable, path], cwd=cwd)  # noqa: S603
    except subprocess.CalledProcessError as e:
        return [e]
    return []


Table = list[list[str | float]]


def compare_tables(
    before: dict[str, Table], after: dict[str, Table]
) -> Iterable[ExceptionGroup[AssertionError]]:
    """Compare 2 path - table mappings, yielding an ExceptionGroup for each file with mismatches."""
    for path, data in before.items():
        exceptions = list(compare_table(data, after[path]))
        if exceptions:
            yield ExceptionGroup(f"path: {path}", exceptions)


def compare_table(ref: Table, test: Table) -> Iterable[AssertionError]:
    """Compare 2 tables, yielding an AssertionError for each line with mismatches.

    Because of the csv files having different column counts per row
    compare line by line.
    """

    ref_row: list[str | float]
    test_row: list[str | float]
    for line_num, (ref_row, test_row) in enumerate(
        itertools.zip_longest(ref, test, fillvalue=[]), start=1
    ):
        if ref_row == test_row:
            continue

        try:
            # Are they different enough to worry about

            np.testing.assert_allclose(
                as_numeric(ref_row),
                as_numeric(test_row),
                atol=1e-4,
                err_msg=f"Mismatch on line: {line_num}",
            )

            # Still good. Check any possible string mismatches

            assert as_str(ref_row) == as_str(test_row), (
                f"Mismatch on line: {line_num}\n+ {ref_row}\n- {test_row}"
            )
        except AssertionError as e:
            yield e


def as_numeric(row: list[str | float]) -> NDArray[np.float64]:
    """Convert a row into numeric values, replacing string values by NaN."""
    return np.array([x if isinstance(x, float) else np.nan for x in row])


def as_str(row: list[str | float]) -> list[str]:
    """Convert a row into str values, replacing numeric values by "#"."""
    return [x if isinstance(x, str) else "#" for x in row]


def read_csv(path: str, delimiter: str = ";") -> list[list[float | str]]:
    """Read a csv, return a nested list of cells converted to a numerical value if possible."""

    def try_float(s: str) -> str | float:
        try:
            return float(s)
        except ValueError:
            return s

    with open(path, encoding="utf-8") as stream:
        return [[try_float(col) for col in row.split(delimiter)] for row in stream]


def diff(path_a: str, path_b: str) -> bool:
    """Print differences between 2 csv files."""
    a = read_csv(path_a)
    b = read_csv(path_b)
    have_mismatches = False
    for mismatch in compare_table(a, b):
        have_mismatches = True
        print(mismatch.args[0], file=sys.stderr)
    return have_mismatches


if __name__ == "__main__":
    main()
