"""Example"""

import csv
import os

import numpy as np

from hvlbuzz.physics import sound
from hvlbuzz.physics.sound import Season, Weather
from hvlbuzz.physics.system import SystemType
from hvlbuzz.physics.tower import Tower

TOWER_FOLDER = os.path.join(os.path.dirname(__file__), "tower-designs")

filenames = [
    "bpa_ostrander_1",
    "bpa_ostrander_2",
    "bpa_ostrander_3",
    "apple_grove_a",
    "apple_grove_b",
    "apple_grove_c",
    "bpa_lyons_8",
    "bpa_lyons_7",
    "project_uhv_8",
    "project_uhv_12",
    "bpa_marion_alvey_lane",
    "bpa_mcnary_ross",
    "bpa_lexington_ross",
    "bpa_oregon_city_keeler",
    "bpa_an_boot",
    "hydro_quebec_1",
    "hydro_quebec_2",
    "puget_power",
    "square_butte",
    "dalles_2",
    "dalles_4",
    "ireq_4",
    "ireq_6",
    "ireq_8",
]

tower = Tower(num_contour=80)


with open("comp_an.csv", "w", newline="", encoding="utf-8") as csvfile:
    writer = csv.writer(csvfile, delimiter=";")

    writer.writerow(["Audible Noise Comparison Data"])
    writer.writerow([])

    writer.writerow(
        [
            "Name",
            "Outer Gradient",
            "Inner Gradient",
            "AC EPRI SPL",
            "AC BPA SPL",
            "DC EPRI SPL",
            "DC BPA SPL",
            "DC CRIEPI ",
        ]
    )

    for filename in filenames:
        tower.load_tower_config(os.path.join(TOWER_FOLDER, filename + ".json"))
        tower.calc_ave_max_conductor_surface_gradient()

        if filename in ["bpa_ostrander_1", "bpa_ostrander_2", "bpa_ostrander_3"]:
            dist_x = 20.1
        elif filename in ["bpa_lexington_ross", "square_butte", "dalles_4"]:
            dist_x = 0
        else:
            dist_x = 15

        lines = tower.systems[0].lines

        system_types = [system.system_type for system in tower.systems]

        if SystemType.ac in system_types:
            outer_gradient = lines[0].E_ac
            inner_gradient = lines[1].E_ac

            pos_phase = np.array([[lines[2].line_x + dist_x], [0.0]])

            ac_weather = Weather.Foul
            an_ac_epri, _ = sound.AcEpri(
                weather=ac_weather,
                altitude=300.0,
                rain_rate=0.8,
                use_efield_independent_rain_correction=True,
            ).calc_systems(tower.systems, pos_phase)
            an_ac_bpa, _ = sound.AcBpa(
                weather=ac_weather,
                altitude=300.0,
                rain_rate=0.8,
                use_efield_independent_rain_correction=True,
            ).calc_systems(tower.systems, pos_phase)

            writer.writerow([filename, outer_gradient, inner_gradient, an_ac_epri[0], an_ac_bpa[0]])

        if SystemType.dc in system_types or SystemType.dc_bipol in system_types:
            outer_gradient = lines[0].E_dc
            inner_gradient = lines[1].E_dc

            pos_phase = np.array([[lines[1].line_x + dist_x], [0.0]])

            dc_weather = Weather.Fair
            if filename in ["square_butte", "dalles_2", "ireq_4", "ireq_6", "ireq_8"]:
                season = Season.Fall
            elif filename in ["dalles_4"]:
                season = Season.Summer
            else:
                season = Season.Spring

            an_dc_epri, _ = sound.DcEpri(
                weather=dc_weather,
                season=season,
                altitude=300.0,
            ).calc_systems(tower.systems, pos_phase)
            an_dc_bpa, _ = sound.DcBpa(
                weather=dc_weather,
                season=season,
                altitude=300.0,
            ).calc_systems(tower.systems, pos_phase)
            an_dc_criepi, _ = sound.DcCriepi().calc_systems(tower.systems, pos_phase)

            writer.writerow(
                [
                    filename,
                    outer_gradient,
                    inner_gradient,
                    "",
                    "",
                    an_dc_epri[0],
                    an_dc_bpa[0],
                    an_dc_criepi[0],
                ]
            )
