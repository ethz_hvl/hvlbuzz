"""Example"""

import csv
import os

import numpy as np

from hvlbuzz.physics import sound
from hvlbuzz.physics.sound import Season, Weather
from hvlbuzz.physics.tower import Tower

TOWER_FOLDER = os.path.join(os.path.dirname(__file__), "tower-designs")

filenames = ["epri_a", "epri_b", "epri_c", "epri_d", "eu_1", "eu_2", "eu_3"]

tower = Tower(num_contour=80)

ground_points = np.vstack((np.arange(-60, 60, 0.1), np.full(1200, 0.0)))

idx_epri = 880
idx_eu = 890

with open("comp_an_bias.csv", "w", newline="", encoding="utf-8") as csvfile:
    writer = csv.writer(csvfile, delimiter=";")

    writer.writerow(["Audible Noise Comparison Data"])
    writer.writerow([])

    writer.writerow(
        [
            "Name",
            "AC EPRI",
            "AC EPRI Bias",
            "AC BPA Bias",
            "DC EPRI",
            "DC CRIEPI",
            "DC BPA",
        ]
    )

    for filename in filenames:
        tower.load_tower_config(os.path.join(TOWER_FOLDER, filename + ".json"))
        tower.calc_ave_max_conductor_surface_gradient()

        if filename in ["epri_a", "epri_b", "epri_c", "epri_d"]:
            idx = idx_epri
        elif filename in ["eu_2", "eu_3"]:
            idx = idx_eu
        elif filename == "eu_1":
            idx = ground_points.shape[1] - idx_eu
        else:
            idx = -1

        ac_weather = Weather.Foul

        an_ac_epri, _ = sound.AcEpri(
            weather=ac_weather,
            altitude=300.0,
            rain_rate=0.8,
            use_efield_independent_rain_correction=True,
        ).calc_systems(
            tower.systems,
            ground_points,
        )

        ac_epri_non_bias = an_ac_epri[idx]

        an_ac_epri, _ = sound.AcEpri(
            weather=ac_weather,
            altitude=300.0,
            rain_rate=0.8,
            use_efield_independent_rain_correction=True,
        ).calc_systems_offset(
            tower.systems,
            ground_points,
        )
        an_ac_bpa, _ = sound.AcBpa(
            weather=ac_weather,
            altitude=300.0,
            rain_rate=0.8,
            use_efield_independent_rain_correction=True,
        ).calc_systems_offset(
            tower.systems,
            ground_points,
        )

        ac_epri_bias = an_ac_epri[idx]
        ac_bpa_bias = an_ac_bpa[idx]

        dc_weather = Weather.Fair
        season = Season.Summer

        an_dc_epri, _ = sound.DcEpri(
            weather=dc_weather, season=season, altitude=300.0
        ).calc_systems(
            tower.systems,
            ground_points,
        )
        an_dc_bpa, _ = sound.DcBpa(weather=dc_weather, season=season, altitude=300.0).calc_systems(
            tower.systems,
            ground_points,
        )
        an_dc_criepi, _ = sound.DcCriepi().calc_systems(
            tower.systems,
            ground_points,
        )

        dc_epri = an_dc_epri[idx]
        dc_criepi = an_dc_criepi[idx]
        dc_bpa = an_dc_bpa[idx]

        writer.writerow(
            [
                filename,
                ac_epri_non_bias,
                ac_epri_bias,
                ac_bpa_bias,
                dc_epri,
                dc_criepi,
                dc_bpa,
            ]
        )
