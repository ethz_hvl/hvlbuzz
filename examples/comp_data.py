"""Example"""

import csv
import os

import numpy as np

from hvlbuzz.physics.line import LineType
from hvlbuzz.physics.tower import Tower

ac_filenames = [
    "ac_1",
    "ac_2",
    "ac_3",
    "ac_4",
    "ac_5",
    "ac_6",
    "ac_7",
    "ac_8",
    "ac_9",
    "ac_10",
    "ac_11",
]

dc_filenames = [
    "dc_1",
    "dc_2",
    "dc_3",
    "dc_4",
    "dc_5",
    "dc_6",
    "dc_7",
    "dc_8",
    "dc_9",
    "dc_10",
    "dc_11",
    "dc_12",
    "dc_13",
    "dc_14",
    "dc_15",
    "dc_16",
    "dc_17",
    "dc_18",
    "dc_19",
    "dc_20",
    "dc_21",
    "dc_22",
    "dc_23",
    "dc_24",
    "dc_25",
    "dc_26",
    "dc_27",
]

hyb_filenames = [
    "hybrid_1",
    "hybrid_2",
    "hybrid_3",
    "hybrid_4",
    "hybrid_5",
    "hybrid_6",
    "hybrid_7",
    "hybrid_8",
    "hybrid_9",
    "hybrid_10",
    "hybrid_11",
    "hybrid_12",
    "hybrid_13",
    "hybrid_14",
    "hybrid_15",
    "hybrid_16",
    "hybrid_17",
    "hybrid_18",
    "hybrid_19",
    "hybrid_20",
    "hybrid_21",
    "hybrid_22",
    "hybrid_23",
    "hybrid_24",
    "hybrid_25",
    "hybrid_26",
    "hybrid_27",
    "hybrid_28",
    "hybrid_29",
    "hybrid_30",
]

tower = Tower(num_contour=80)

TOWER_FOLDER = os.path.join(os.path.dirname(__file__), "tower-designs", "guillod")

with open("comp_data.csv", "w", newline="", encoding="utf-8") as csvfile:
    writer = csv.writer(csvfile, delimiter=";")

    writer.writerow(["AC"])
    writer.writerow([])

    writer.writerow(
        [
            "R1_X",
            "R1_Y",
            "S1_X",
            "S1_Y",
            "T1_X",
            "T1_Y",
            "R2_X",
            "R2_Y",
            "S2_X",
            "S2_Y",
            "T2_X",
            "T2_Y",
            "GND_X",
            "GND_Y",
            "E_dc_max",
            "E_ac_max",
            "E_sum_max",
        ]
    )

    for filename in ac_filenames:
        tower.load_tower_config(os.path.join(TOWER_FOLDER, filename + ".json"))
        tower.calc_electric_field(np.arange(-60, 60, 0.1), 1)

        coord = []
        for system in tower.systems:
            for line in system.lines:
                coord.append(line.line_x)
                coord.append(line.line_y)

        E_ac_max = np.max(tower.E_ac_ground)
        E_dc_max = np.max(tower.E_dc_ground)
        E_sum_max = np.max(np.hypot(tower.E_ac_ground, tower.E_dc_ground))
        coord.extend([E_dc_max, E_ac_max, E_sum_max])

        writer.writerow(coord)

    writer.writerow([])
    writer.writerow(["DC"])
    writer.writerow([])

    writer.writerow(
        [
            "+1_X",
            "+1_Y",
            "-1_X",
            "-1_Y",
            "N1_X",
            "N1_Y",
            "+2_X",
            "+2_Y",
            "-2_X",
            "-2_Y",
            "N2_X",
            "N2_Y",
            "GND_X",
            "GND_Y",
            "E_dc_max",
            "E_ac_max",
            "E_sum_max",
        ]
    )

    for filename in dc_filenames:
        tower.load_tower_config(os.path.join(TOWER_FOLDER, filename + ".json"))
        tower.calc_electric_field(np.arange(-60, 60, 0.1), 1)

        coord = []
        for system in tower.systems:
            for line in system.lines:
                if line.line_type != LineType.gnd:
                    coord.append(line.line_x)
                    coord.append(line.line_y)

        E_ac_max = np.max(tower.E_ac_ground)
        E_dc_max = np.max(tower.E_dc_ground)
        E_sum_max = np.max(np.hypot(tower.E_ac_ground, tower.E_dc_ground))
        coord.extend([E_dc_max, E_ac_max, E_sum_max])

        writer.writerow(coord)

    writer.writerow([])
    writer.writerow(["Hybrid"])
    writer.writerow([])

    writer.writerow(
        [
            "R1_X",
            "R1_Y",
            "S1_X",
            "S1_Y",
            "T1_X",
            "T1_Y",
            "+2_X",
            "+2_Y",
            "-2_X",
            "-2_Y",
            "N2_X",
            "N2_Y",
            "GND_X",
            "GND_Y",
            "E_dc_max",
            "E_ac_max",
            "E_sum_max",
        ]
    )

    for filename in hyb_filenames:
        tower.load_tower_config(os.path.join(TOWER_FOLDER, filename + ".json"))
        tower.calc_electric_field(np.arange(-60, 60, 0.1), 1)

        coord = []
        for system in tower.systems:
            for line in system.lines:
                coord.append(line.line_x)
                coord.append(line.line_y)

        E_ac_max = np.max(tower.E_ac_ground)
        E_dc_max = np.max(tower.E_dc_ground)
        E_sum_max = np.max(np.hypot(tower.E_ac_ground, tower.E_dc_ground))
        coord.extend([E_dc_max, E_ac_max, E_sum_max])

        writer.writerow(coord)

    writer.writerow([])
    writer.writerow(["AC"])
    writer.writerow([])

    writer.writerow(["B_dc_max", "B_ac_max", "B_sum_max"])

    for filename in ac_filenames:
        tower.load_tower_config(os.path.join(TOWER_FOLDER, filename + ".json"))
        tower.calc_magnetic_field(np.arange(-60, 60, 0.1), 1)
        B_ac_max = np.max(tower.B_ac)
        B_dc_max = np.max(tower.B_dc)
        B_sum_max = np.max(np.hypot(tower.B_ac, tower.B_dc))
        writer.writerow([B_dc_max, B_ac_max, B_sum_max])

    writer.writerow([])
    writer.writerow(["DC"])
    writer.writerow([])

    writer.writerow(["B_dc_max", "B_ac_max", "B_sum_max"])

    for filename in dc_filenames:
        tower.load_tower_config(os.path.join(TOWER_FOLDER, filename + ".json"))
        tower.calc_magnetic_field(np.arange(-60, 60, 0.1), 1)
        B_ac_max = np.max(tower.B_ac)
        B_dc_max = np.max(tower.B_dc)
        B_sum_max = np.max(np.hypot(tower.B_ac, tower.B_dc))
        writer.writerow([B_dc_max, B_ac_max, B_sum_max])

    writer.writerow([])
    writer.writerow(["Hybrid"])
    writer.writerow([])

    writer.writerow(["B_dc_max", "B_ac_max", "B_sum_max"])

    for filename in hyb_filenames:
        tower.load_tower_config(os.path.join(TOWER_FOLDER, filename + ".json"))
        tower.calc_magnetic_field(np.arange(-60, 60, 0.1), 1)
        B_ac_max = np.max(tower.B_ac)
        B_dc_max = np.max(tower.B_dc)
        B_sum_max = np.max(np.hypot(tower.B_ac, tower.B_dc))
        writer.writerow([B_dc_max, B_ac_max, B_sum_max])
