"""Example"""

import csv
import os
import time
from collections.abc import Iterable

import numpy as np
from numpy.typing import NDArray

from hvlbuzz.physics.line import LineType
from hvlbuzz.physics.system import SystemType
from hvlbuzz.physics.tower import EPSILON_0, Tower

TOWER_FOLDER = os.path.join(os.path.dirname(__file__), "tower-designs")


class ConvergenceTower(Tower):
    """Overload of a tower which can calculate surface potentials."""

    def __init__(self) -> None:
        super().__init__()
        self.num_contour = 0

    def set_num_contour(self, num_contour: int) -> None:
        """Set number of contours."""
        self.num_contour = num_contour

    def calc_surface_potential(self) -> tuple[list[float], list[float]]:
        """Compute the electric field on the ground  level below the high voltage line conductors.

        The Charge Simulation Method
        (CSM) is used for the calculation of electric field.

        input variable(s):
        - ground_points: Numpy array containing x-coordinates of ground points
        - height_above_ground: height of ground points

        The calculation will be saved as class variable:
        - E_ac_ground: electric field caused by AC conductors [kV/cm]
        - E_dc_ground: electric field caused by DC conductors [kV/cm]

        The return value is a boolean status signaling the success of the
        calculation:
        - False: calculation aborted
        - True: calculation successful
        """

        # get the coordinates of contours, charges and mirror charges
        points = self.prepare_positions()
        contours = points.contours
        charges = np.array(points.charges)
        mirror_charges = np.array(points.mirror_charges)

        # calculate the matrices
        pot_matrix, *_ = self.calc_matrices(contours, charges, mirror_charges)

        # calculate voltages
        U_ac, U_dc = self.calc_voltages()

        line_charges_ac: NDArray[np.complex128] = np.linalg.solve(pot_matrix, U_ac)
        line_charges_dc = np.linalg.solve(pot_matrix, U_dc)

        # Prepare angles for the conductor surface coordinates:
        num_points = 1000
        phi = 2 * np.pi * np.arange(num_points) / num_points
        ac_errors = []
        dc_errors = []
        for system in self.systems:
            for line in system.lines:
                for con in line.cons:
                    # get system voltage
                    voltage = system.voltage

                    # get radius and coordinates of first conductor
                    radius = line.con_radius
                    con_pos = np.array([con.con_x, con.con_y])

                    # create the array of conductor surface coordinates
                    coords = con_pos + radius * np.column_stack((np.cos(phi), np.sin(phi)))

                    # calculate the difference between contour points and each of
                    # simulated charges points and mirror charge points
                    # with shape (num_points, n_charges, 2)
                    charges_diff = coords[:, None, :] - charges
                    mirror_charges_diff = coords[:, None, :] - mirror_charges

                    # calculate the euclidean distances
                    charges_norm = np.linalg.norm(charges_diff, axis=2)
                    mirror_charges_norm = np.linalg.norm(mirror_charges_diff, axis=2)

                    con_error = np.nan
                    if system.system_type == SystemType.ac:
                        con_error = compute_err_ac(
                            voltage, charges_norm, mirror_charges_norm, line_charges_ac
                        )
                    elif system.system_type in {SystemType.dc, SystemType.dc_bipol}:
                        con_error = compute_err_dc(
                            voltage,
                            charges_norm,
                            mirror_charges_norm,
                            line_charges_dc,
                            line.line_type,
                        )
                    if line.line_type in {LineType.ac_r, LineType.ac_s, LineType.ac_t}:
                        ac_errors.append(con_error)
                    if line.line_type in {LineType.dc_pos, LineType.dc_neg}:
                        dc_errors.append(con_error)
        return ac_errors, dc_errors


# Constant coefficient for electric field calculation
COEFF = 1.0 / (2 * np.pi * EPSILON_0)


def compute_err_ac(
    voltage: float,
    charges_norm: float,
    mirror_charges_norm: float,
    line_charges_ac: NDArray[np.complex128],
) -> float:
    """Comupute AC errors."""
    con_V = COEFF * np.abs(
        np.log(mirror_charges_norm / charges_norm) @ line_charges_ac,
    )
    err: float = np.abs((con_V - voltage / np.sqrt(3)) / (voltage / np.sqrt(3)) * 100)
    return err


def compute_err_dc(
    voltage: float,
    charges_norm: float,
    mirror_charges_norm: float,
    line_charges_dc: NDArray[np.float64],
    line_type: LineType,
) -> float:
    """Comupute DC errors."""
    con_V = COEFF * np.log(mirror_charges_norm / charges_norm) @ line_charges_dc

    err = np.nan
    if line_type == LineType.dc_pos:
        err = np.abs((con_V - voltage) / voltage * 100)
    elif line_type == LineType.dc_neg:
        err = np.abs((con_V + voltage) / voltage * 100)
    elif line_type == LineType.dc_neut:
        err = np.abs(con_V)
    return err


def collect_surface_gradient(tower: Tower) -> Iterable[float]:
    """Collect surface gradient of lines."""
    for system in tower.systems:
        for line in system.lines:
            if system.system_type == SystemType.ac:
                yield line.E_ac
            if system.system_type in {SystemType.dc, SystemType.dc_bipol}:
                yield line.E_dc


FILENAMES = [
    "ac_1",
    "ac_2",
    "ac_3",
    "ac_4",
    "ac_5",
    "ac_6",
    "ac_7",
    "ac_8",
    "ac_9",
    "ac_10",
    "ac_11",
    "dc_1",
    "dc_2",
    "dc_3",
    "dc_4",
    "dc_5",
    "dc_6",
    "dc_7",
    "dc_8",
    "dc_9",
    "dc_10",
    "dc_11",
    "dc_12",
    "dc_13",
    "dc_14",
    "dc_15",
    "dc_16",
    "dc_17",
    "dc_18",
    "dc_19",
    "dc_20",
    "dc_21",
    "dc_22",
    "dc_23",
    "dc_24",
    "dc_25",
    "dc_26",
    "dc_27",
    "hybrid_1",
    "hybrid_2",
    "hybrid_3",
    "hybrid_4",
    "hybrid_5",
    "hybrid_6",
    "hybrid_7",
    "hybrid_8",
    "hybrid_9",
    "hybrid_10",
    "hybrid_11",
    "hybrid_12",
    "hybrid_13",
    "hybrid_14",
    "hybrid_15",
    "hybrid_16",
    "hybrid_17",
    "hybrid_18",
    "hybrid_19",
    "hybrid_20",
    "hybrid_21",
    "hybrid_22",
    "hybrid_23",
    "hybrid_24",
    "hybrid_25",
    "hybrid_26",
    "hybrid_27",
    "hybrid_28",
    "hybrid_29",
    "hybrid_30",
    "bpa_ostrander_1",
    "bpa_ostrander_2",
    "bpa_ostrander_3",
    "apple_grove_a",
    "apple_grove_b",
    "apple_grove_c",
    "bpa_lyons_8",
    "bpa_lyons_7",
    "project_uhv_8",
    "project_uhv_12",
    "bpa_marion_alvey_lane",
    "bpa_mcnary_ross",
    "bpa_oregon_city_keeler",
    "bpa_an_boot",
    "hydro_quebec_1",
    "hydro_quebec_2",
    "puget_power",
    "square_butte",
    "dalles_2",
    "dalles_4",
    "ireq_4",
    "ireq_6",
    "ireq_8",
    "epri_a",
    "epri_b",
    "epri_c",
    "epri_d",
    "eu_1",
    "eu_2",
    "eu_3",
]

NUM_CONTOUR_CONVERGE = 0
NUM_CONTOUR_RANGE = range(2, 101)


def main() -> None:
    """Entry point."""

    # if run as a test by setting the TEST environment variable,
    # reduce number of contour points:
    num_contour_range = range(2, 4) if "TEST" in os.environ else NUM_CONTOUR_RANGE

    tower = ConvergenceTower()

    with (
        open("convergence_time.csv", "w", newline="", encoding="utf-8") as convergence_time_file,
        open(
            "num_contour_converge.csv", "w", newline="", encoding="utf-8"
        ) as num_contour_converge_file,
        open("max_error_ac.csv", "w", newline="", encoding="utf-8") as max_error_ac_file,
        open("max_error_dc.csv", "w", newline="", encoding="utf-8") as max_error_dc_file,
        open("surface_gradient.csv", "w", newline="", encoding="utf-8") as surface_gradient_file,
    ):
        convergence_time_writer = csv.writer(convergence_time_file, delimiter=";")
        convergence_time_writer.writerow(["filename", *num_contour_range])

        num_contour_converge_writer = csv.writer(num_contour_converge_file, delimiter=";")
        num_contour_converge_writer.writerow(["filename", "num_contour_converge"])

        max_error_ac_writer = csv.writer(max_error_ac_file, delimiter=";")
        max_error_ac_writer.writerow(["filename", *num_contour_range])

        max_error_dc_writer = csv.writer(max_error_dc_file, delimiter=";")
        max_error_dc_writer.writerow(["filename", *num_contour_range])

        surface_gradient_writer = csv.writer(surface_gradient_file, delimiter=";")
        surface_gradient_writer.writerow(["filename", "phase", *num_contour_range])

        for filename in FILENAMES:
            try:
                tower.load_tower_config(os.path.join(TOWER_FOLDER, filename + ".json"))
            except FileNotFoundError:
                tower.load_tower_config(os.path.join(TOWER_FOLDER, "guillod", filename + ".json"))

            print("working on:", filename)

            ave_error_ac, max_error_ac, ave_error_dc, max_error_dc, time_csm = (
                [],
                [],
                [],
                [],
                [],
            )

            line_types = [
                line.line_type.name
                for system in tower.systems
                if system.system_type in {SystemType.ac, SystemType.dc, SystemType.dc_bipol}
                for line in system.lines
            ]
            surface_gradient = [[filename] * len(line_types), line_types]

            for num_contour in num_contour_range:
                print("num contour points:", num_contour)

                tower.set_num_contour(num_contour)
                error_ac, error_dc = tower.calc_surface_potential()

                try:
                    ave_error_ac.append(np.mean(error_ac))
                    max_error_ac.append(np.max(error_ac))
                except ValueError:
                    pass

                try:
                    ave_error_dc.append(np.mean(error_dc))
                    max_error_dc.append(np.max(error_dc))
                except ValueError:
                    pass

                time_iters = []
                surface_gradient_iters = []
                for _ in range(1):
                    start = time.time()
                    tower.calc_ave_max_conductor_surface_gradient()
                    end = time.time()
                    time_iters.append(end - start)

                    surface_gradient_iters.append(list(collect_surface_gradient(tower)))

                time_csm.append(np.average(time_iters))
                surface_gradient.append(np.average(surface_gradient_iters, axis=0))

            convergence_time_writer.writerow([filename, *time_csm])
            num_contour_converge_writer.writerow([filename, NUM_CONTOUR_CONVERGE])
            max_error_ac_writer.writerow([filename, *max_error_ac])
            max_error_dc_writer.writerow([filename, *max_error_dc])
            for row in map(list, zip(*surface_gradient)):
                surface_gradient_writer.writerow(row)


if __name__ == "__main__":
    main()
