"""Collection of tower configurations for testing."""

from typing import Any

from hvlbuzz.physics.line import Line, LineType
from hvlbuzz.physics.system import System, SystemType


def create_line(
    E_ac: float,
    E_ac_pos_offset: float,
    E_ac_neg_offset: float,
    E_dc: float = 0.0,
    E_dc_rip: float = 0.0,
    **kwargs: Any,
) -> Line:
    """Create a line with precomputed E values."""
    line = Line(**kwargs)
    line.E_ac = E_ac
    line.E_ac_pos_offset = E_ac_pos_offset
    line.E_ac_neg_offset = E_ac_neg_offset
    line.E_dc = E_dc
    line.E_dc_rip = E_dc_rip
    return line


def create_380_kv_ac_systems() -> list[System]:
    """Create AC system instances for testing."""
    return [
        System(
            system_type=SystemType.ac,
            voltage=380000.0,
            current=1000.0,
            lines=[
                create_line(
                    line_type=LineType.ac_r,
                    line_x=-8.0,
                    line_y=40.0,
                    con_radius=0.015,
                    num_con=2,
                    bundle_radius=0.2,
                    con_angle_offset=0.0,
                    E_ac=15.98,
                    E_ac_pos_offset=15.98,
                    E_ac_neg_offset=15.98,
                ),
                create_line(
                    line_type=LineType.ac_s,
                    line_x=-11.0,
                    line_y=30.0,
                    con_radius=0.015,
                    num_con=2,
                    bundle_radius=0.2,
                    con_angle_offset=0.0,
                    E_ac=16.42,
                    E_ac_pos_offset=16.42,
                    E_ac_neg_offset=16.42,
                ),
                create_line(
                    line_type=LineType.ac_t,
                    line_x=-9.0,
                    line_y=20.0,
                    con_radius=0.015,
                    num_con=2,
                    bundle_radius=0.2,
                    con_angle_offset=0.0,
                    E_ac=16.0,
                    E_ac_pos_offset=16.0,
                    E_ac_neg_offset=16.0,
                ),
            ],
        ),
        System(
            system_type=SystemType.ac,
            voltage=380000.0,
            current=1500.0,
            lines=[
                create_line(
                    line_type=LineType.ac_r,
                    line_x=9.0,
                    line_y=20.0,
                    con_radius=0.015,
                    num_con=2,
                    bundle_radius=0.2,
                    con_angle_offset=0.0,
                    E_ac=16.0,
                    E_ac_pos_offset=16.0,
                    E_ac_neg_offset=16.0,
                ),
                create_line(
                    line_type=LineType.ac_s,
                    line_x=11.0,
                    line_y=30.0,
                    con_radius=0.015,
                    num_con=2,
                    bundle_radius=0.2,
                    con_angle_offset=0.0,
                    E_ac=16.42,
                    E_ac_pos_offset=16.42,
                    E_ac_neg_offset=16.42,
                ),
                create_line(
                    line_type=LineType.ac_t,
                    line_x=8.0,
                    line_y=40.0,
                    con_radius=0.015,
                    num_con=2,
                    bundle_radius=0.2,
                    con_angle_offset=0.0,
                    E_ac=15.98,
                    E_ac_pos_offset=15.98,
                    E_ac_neg_offset=15.98,
                ),
            ],
        ),
        System(
            system_type=SystemType.gnd,
            voltage=0.0,
            current=0.0,
            lines=[
                create_line(
                    line_type=LineType.gnd,
                    line_x=0.0,
                    line_y=50.0,
                    con_radius=0.01,
                    num_con=1,
                    bundle_radius=0.0,
                    con_angle_offset=0.0,
                    E_ac=2.19,
                    E_ac_pos_offset=2.19,
                    E_ac_neg_offset=2.19,
                )
            ],
        ),
    ]


def create_420_kv_ac_systems() -> list[System]:
    """Create AC system instances for testing."""
    return [
        System(
            system_type=SystemType.ac,
            voltage=420000.0,
            current=2530.0,
            lines=[
                create_line(
                    line_type=LineType.ac_r,
                    line_x=8.0,
                    line_y=15.0,
                    con_radius=0.01595,
                    num_con=2,
                    bundle_radius=0.2,
                    con_angle_offset=0.0,
                    E_ac=17.06566133,
                    E_ac_pos_offset=17.065661329356534,
                    E_ac_neg_offset=17.065661329356534,
                ),
                create_line(
                    line_type=LineType.ac_s,
                    line_x=10.0,
                    line_y=24.3,
                    con_radius=0.01595,
                    num_con=2,
                    bundle_radius=0.2,
                    con_angle_offset=0.0,
                    E_ac=17.18384275,
                    E_ac_pos_offset=17.1838427467931,
                    E_ac_neg_offset=17.1838427467931,
                ),
                create_line(
                    line_type=LineType.ac_t,
                    line_x=7.0,
                    line_y=35.3,
                    con_radius=0.01595,
                    num_con=2,
                    bundle_radius=0.2,
                    con_angle_offset=0.0,
                    E_ac=16.29527149,
                    E_ac_pos_offset=16.29527148926104,
                    E_ac_neg_offset=16.29527148926104,
                ),
            ],
        ),
        System(
            system_type=SystemType.ac,
            voltage=245000.0,
            current=1950.0,
            lines=[
                create_line(
                    line_type=LineType.ac_r,
                    line_x=-7.0,
                    line_y=37.1,
                    con_radius=0.01305,
                    num_con=2,
                    bundle_radius=0.2,
                    con_angle_offset=0.0,
                    E_ac=12.08539507,
                    E_ac_pos_offset=12.085395065388896,
                    E_ac_neg_offset=12.085395065388896,
                ),
                create_line(
                    line_type=LineType.ac_s,
                    line_x=-10.0,
                    line_y=26.1,
                    con_radius=0.01305,
                    num_con=2,
                    bundle_radius=0.2,
                    con_angle_offset=0.0,
                    E_ac=11.87845342,
                    E_ac_pos_offset=11.878453416192247,
                    E_ac_neg_offset=11.878453416192247,
                ),
                create_line(
                    line_type=LineType.ac_t,
                    line_x=-8.0,
                    line_y=16.8,
                    con_radius=0.01305,
                    num_con=2,
                    bundle_radius=0.2,
                    con_angle_offset=0.0,
                    E_ac=11.97188224,
                    E_ac_pos_offset=11.971882243684982,
                    E_ac_neg_offset=11.971882243684982,
                ),
            ],
        ),
        System(
            system_type=SystemType.gnd,
            voltage=0.0,
            current=0.0,
            lines=[
                create_line(
                    line_type=LineType.gnd,
                    line_x=0.0,
                    line_y=52.6,
                    con_radius=0.015,
                    num_con=1,
                    bundle_radius=0.0,
                    con_angle_offset=0.0,
                    E_ac=1.33279722,
                    E_ac_pos_offset=1.332797218575698,
                    E_ac_neg_offset=1.332797218575698,
                )
            ],
        ),
    ]


def create_hybrid_systems() -> list[System]:
    """Create AC and DC system instances for testing."""
    return [
        System(
            system_type=SystemType.ac,
            voltage=420000.0,
            current=2530.0,
            lines=[
                create_line(
                    line_type=LineType.ac_r,
                    line_x=8.0,
                    line_y=15.0,
                    con_radius=0.01595,
                    num_con=2,
                    bundle_radius=0.2,
                    con_angle_offset=0.0,
                    E_ac=16.81970298,
                    E_ac_pos_offset=17.07614880105428,
                    E_ac_neg_offset=17.07614880105428,
                    E_dc=0.36266916,
                    E_dc_rip=17.182372137785922,
                ),
                create_line(
                    line_type=LineType.ac_s,
                    line_x=10.0,
                    line_y=24.3,
                    con_radius=0.01595,
                    num_con=2,
                    bundle_radius=0.2,
                    con_angle_offset=0.0,
                    E_ac=17.1832685,
                    E_ac_pos_offset=17.20985729355482,
                    E_ac_neg_offset=17.20985729355482,
                    E_dc=-0.03760223,
                    E_dc_rip=17.220870732931328,
                ),
                create_line(
                    line_type=LineType.ac_t,
                    line_x=7.0,
                    line_y=35.3,
                    con_radius=0.01595,
                    num_con=2,
                    bundle_radius=0.2,
                    con_angle_offset=0.0,
                    E_ac=15.77222722,
                    E_ac_pos_offset=16.75336079590271,
                    E_ac_neg_offset=16.75336079590271,
                    E_dc=-1.38753241,
                    E_dc_rip=17.159759630560814,
                ),
            ],
        ),
        System(
            system_type=SystemType.gnd,
            voltage=0.0,
            current=0.0,
            lines=[
                create_line(
                    line_type=LineType.gnd,
                    line_x=0.0,
                    line_y=52.6,
                    con_radius=0.015,
                    num_con=1,
                    bundle_radius=0.0,
                    con_angle_offset=0.0,
                    E_ac=2.3107663,
                    E_ac_pos_offset=4.730381435090111,
                    E_ac_neg_offset=4.730381435090111,
                    E_dc=-3.42185254,
                    E_dc_rip=5.732618839426133,
                )
            ],
        ),
        System(
            system_type=SystemType.dc,
            voltage=400000.0,
            current=2000.0,
            lines=[
                create_line(
                    line_type=LineType.dc_pos,
                    line_x=-7.0,
                    line_y=37.1,
                    con_radius=0.01305,
                    num_con=2,
                    bundle_radius=0.2,
                    con_angle_offset=0.0,
                    E_ac=1.74476778,
                    E_ac_pos_offset=24.546956036388444,
                    E_ac_neg_offset=24.546956036388444,
                    E_dc=32.24716388,
                    E_dc_rip=33.99193166392762,
                ),
                create_line(
                    line_type=LineType.dc_neg,
                    line_x=-10.0,
                    line_y=26.1,
                    con_radius=0.01305,
                    num_con=2,
                    bundle_radius=0.2,
                    con_angle_offset=0.0,
                    E_ac=0.45579573,
                    E_ac_pos_offset=24.04026290316959,
                    E_ac_neg_offset=24.04026290316959,
                    E_dc=-33.35347334,
                    E_dc_rip=33.80926906820714,
                ),
                create_line(
                    line_type=LineType.dc_neut,
                    line_x=-8.0,
                    line_y=16.8,
                    con_radius=0.01305,
                    num_con=2,
                    bundle_radius=0.2,
                    con_angle_offset=0.0,
                    E_ac=0.60808494,
                    E_ac_pos_offset=3.1020462151565193,
                    E_ac_neg_offset=3.1020462151565193,
                    E_dc=3.52699386,
                    E_dc_rip=4.13507879839789,
                ),
            ],
        ),
    ]
